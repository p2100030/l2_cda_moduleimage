#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include "Image.h"

using namespace std;

Image::Image (): dimx(0), dimy(0), tab(nullptr) {}

Image::Image (unsigned int dimensionX, unsigned int dimensionY) {
        assert(dimensionX != 0);
        assert(dimensionY != 0);
        dimx = dimensionX; dimy = dimensionY;
        tab = new Pixel [dimensionX*dimensionY];
}

Image::~Image () {
    delete [] tab;
    tab = nullptr;
    dimx = 0; dimy = 0;
}

unsigned int Image::getDimx () {
    return dimx;
}

unsigned int Image::getDimy () {
    return dimy;
}

Pixel & Image::getPix (unsigned int x, unsigned int y) {
    assert (x < dimx);
    assert (y < dimy);
    return tab[y*dimx + x];
}

Pixel Image::getPix (unsigned int x, unsigned int y) const {
    assert (x < dimx);
    assert (y < dimy);
    return tab[y*dimx + x];
}

void Image::setPix (unsigned int x, unsigned int y, Pixel couleur) {
    assert (x < dimx);
    assert (y < dimy);
    tab[y*dimx + x] = couleur;
}

void Image::dessinerRectangle (unsigned int Xmin, unsigned int Ymin,
unsigned int Xmax, unsigned int Ymax, Pixel couleur) {
    assert (Xmin < dimx);
    assert (Ymin < dimy);
    assert (Xmax <= dimx);
    assert (Ymax <= dimy);
    for (unsigned int i = Xmin; i < Xmax; ++i)
        for (unsigned int j = Ymin; j < Ymax; ++j)
            setPix(i, j, couleur);

}

void Image::effacer (Pixel couleur) {
    dessinerRectangle(0, 0, dimx, dimy, couleur);
}

void Image::testRegression () {
    /** Test des constructeurs **/
    Image a;
    assert(a.dimx == 0);
    assert(a.dimy == 0);
    Image b (8, 4);
    assert(b.dimx == 8);
    assert(b.dimy == 4);

    /** Test des fonctions get et set **/
    Pixel rouge (125, 0, 0);
    Pixel & p1 = b.getPix(2, 1); // Référence sur le pixel (2, 1) de l'image b
    b.setPix(2, 1, rouge);  // Modification du pixel (2, 1) de l'image b
    Pixel p2 = b.getPix(2, 1); // Copie du pixel (2, 1) de l'image b
    // Vérifie que p1 = p2
    assert(p1.r == p2.r);
    assert(p1.g == p2.g);
    assert(p1.b == p2.b);
    

    /** Test des fonctions dessiner et effacer **/
    Pixel orange (255, 150, 0);
    Pixel blanc (255, 255, 255);
    b.dessinerRectangle (1, 1, 3, 3, orange);
    // Verifie que les pixels du petit rectangle sont orange.
    for (unsigned int i = 1; i < 3; ++i) 
        for (unsigned int j = 1; j < 3; ++j) {
            Pixel p = b.getPix(i, j);
            assert(p.r == 255);
            assert(p.g == 150);
            assert(p.b == 0);
        }
    // Verifie que l'image est blanche
    b.effacer(blanc);
    for (unsigned int i = 0; i < b.dimx; ++i) 
        for (unsigned int j = 0; j < b.dimy; ++j) {
            Pixel p = b.getPix(i, j);
            assert(p.r == 255);
            assert(p.g == 255);
            assert(p.b == 255);
        }
}

void Image::sauver (const string & filename) const {
    ofstream fichier (filename.c_str(), ios::trunc);
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        fichier << endl;
    }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir (const string & filename) {
    ifstream fichier (filename.c_str());
    assert(fichier.is_open());
    unsigned char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr) delete[] tab;
    tab = new Pixel [dimx*dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> g >> b;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole () const {
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            cout << int(pix.r) << " " << int(pix.g) << " " << int(pix.b) << " ";
        }
        cout << endl;
    }
}