#ifndef _IMAGE_VIEWER_H
#define _IMAGE_VIEWER_H

#include <SDL2/SDL.h>
#include "Image.h"

/**
 * @class ImageViewer
 * 
 * @brief Représente une image grace à un tableau de pixel.
*/
class ImageViewer {
    private:
        SDL_Window * window;
        SDL_Renderer * renderer;
        SDL_Surface * surface;
        SDL_Texture * texture;

    
    public:
        // DOXY
        ImageViewer ();

        //DOXY
        ~ImageViewer ();

        //DOXY
        void afficher (Image im);

};

#endif