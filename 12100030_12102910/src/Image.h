#ifndef _IMAGE_H
#define _IMAGE_H

#include <string>
#include "Pixel.h"

/**
 * @class Image
 * 
 * @brief Représente une image grace à un tableau de pixel.
*/
class Image {
    private:
        Pixel * tab; ///< Tableau 1D de pixel.
        unsigned int dimx, dimy; ///< Dimension de l'image.
    
    public:
        /**
         * @brief Constructeur par defaut.
         * 
         * Crée une instance Image avec les valeurs par defaut.
        */
        Image ();
        
        /**
         * @brief Constructeur avec paramétres.
         * 
         * @param dimensionX Dimension X de l'image.
         * @param dimensionY  Dimension  Y de l'image.
        */
        Image (unsigned int dimensionX, unsigned int dimensionY);

        /**
         * @brief Destructeur.
         * 
         * Désalocation du tableau de pixel et mise à jour des champs dimx et dimy à 0.
        */
        ~Image ();

        /**
         * @brief Récupère la dimension y de l'image.
         * 
         * @return Dimension x de l'image.
        */
        unsigned int getDimx ();

        /**
         * @brief Récupère la dimension y de l'image.
         * 
         * @return Dimension y de l'image.
        */
        unsigned int getDimy ();

        /**
         * @brief Récupère le pixel original de coordonnées (x,y).
         * 
         * @param x Coordonnées x du pixel.
         * @param y Coordonnées y du pixel.
         * @return Référence sur le Pixel.
        */
        Pixel & getPix (unsigned int x, unsigned int y);

        /**
         * @brief Recupere une copie du Pixel original de cordonnée (x,y).
         * 
         * @param x Coordonnées x du pixel.
         * @param y Coordonnées y du pixel.
         * @return Copie du Pixel.
        */
        Pixel getPix (unsigned int x, unsigned int y) const;

        //Modifie la couleur du pixel de coordonnées (x,y) - DOXY
        void setPix (unsigned int x, unsigned int y, Pixel couleur);

        /**
         * @brief dessine un rectangle plein de la couleur en paramétres dans l'image.
         * 
         * @param Xmin Coordonnées Xmin du Rectangle
         * @param Ymin Coordonnées Ymin du Rectangle.
         * @param Xmax Coordonnées Xmax du Rectangle.
         * @param Ymin Coordonnées Ymin du Rectangle.
         * @param Couleur Couleur du rectangle.  
        */
        void dessinerRectangle (unsigned int Xmin, unsigned int Ymin,
        unsigned int Xmax, unsigned int Ymax, Pixel couleur);

        /**
         * @brief effacer l'image en la remplissant de la couleur donnée .
         * 
         * @param couleur Couleur qui rempli l'image.
        */
        void effacer (Pixel couleur);

        /**
         * @brief Effectue une serie de tests qui verifie le bon fonctionnement de toutes les fonctions.
         *
        */
        static void testRegression ();
 
        /**
         * @brief Permet d'ecrire l'image dans un fichier de sortie au format ppm.
         * 
         * @param filename chemin absolue ou relatif vers le fichiers de sortie en format ppm.
        */
        void sauver (const std::string & filename) const;

        /**
         * @brief Permet lire l'image d'un fichier au format ppm et de le mettre dans l'image.
         * 
         * @param filename chemin absolue ou relatif vers le fichiers de lecture au format ppm.
        */
        void ouvrir (const std::string & filename);

        /**
         * @brief Permet d'afficher l'image sur la console.
        */
        void afficherConsole () const;

};

#endif