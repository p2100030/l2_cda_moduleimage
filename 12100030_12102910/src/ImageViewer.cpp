#include <SDL2/SDL.h>
#include <cassert>
#include "ImageViewer.h"

ImageViewer::ImageViewer () : window(nullptr), renderer(nullptr), surface(nullptr), texture(nullptr) {
    //Dimension de la fenetre
    int w, h;
    w = 200; h = 200;

    // Initialisation
    assert(SDL_Init(SDL_INIT_VIDEO) == 0);

    // Création de la fenêtre
    window = SDL_CreateWindow("Image Viewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    w, h, SDL_WINDOW_SHOWN);
    assert(window != nullptr);

    // Création du renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    assert(renderer != nullptr);

     SDL_GetWindowSize(window, &w, &h);

    // Initialisation de la surface
    surface = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);
    assert(surface != nullptr);

    // Initialisation de la texture
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    assert(texture != nullptr);
}

ImageViewer::~ImageViewer () {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);

    window = nullptr;
    renderer = nullptr;
    surface = nullptr;
    texture = nullptr;

    SDL_Quit();
}

void ImageViewer::afficher (Image im) {
    // Convertie l'image en une surface SDL
    SDL_Surface * newSurface = SDL_CreateRGBSurface(0, im.getDimx(), im.getDimy(), 32, 0, 0, 0, 0);
    assert(surface != nullptr);

    // Remplie la surface SDL avec les pixels de l'image
    for (unsigned int y = 0; y < im.getDimy(); ++y) {
        for (unsigned int x = 0; x < im.getDimx(); ++x) {
            // Récupére le pixel de l'image
            Pixel pixel = im.getPix(x, y);
            // Convertie la couleur du pixel en format SDL
            Uint32 sdlColor = SDL_MapRGB(surface->format, pixel.r, pixel.g, pixel.b);
            // Placee le pixel sur la surface SDL
            SDL_Rect rect = { int(x), int(y), 1, 1 }; // position et taille du pixel
            SDL_FillRect(surface, &rect, sdlColor);
        }
    }

    // Mise à jour de la texture avec la nouvelle surface
    SDL_DestroyTexture(texture);
    texture = SDL_CreateTextureFromSurface(renderer, newSurface);
    assert(texture != nullptr);
    
    // Libérer la mémoire de la surface précedente
    SDL_FreeSurface(surface);
    surface = newSurface;
    
    // Effacer l'écran
    SDL_RenderClear(renderer);
    // Afficher la texture
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}