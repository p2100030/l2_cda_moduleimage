#ifndef _PIXEL_H
#define _PIXEL_H

/**
 * @struct Pixel
 * 
 * @brief Représente un pixel d'une image avec les composantes RVB.
*/
struct Pixel {
    unsigned char r, g, b; ///< Les composantes du pixel, entier entre 0 et 255.
    
    /**
     * @brief Constructeur par défaut.
    */
    Pixel ();
    
    /**
     * @brief Constructeur avec paramétres.
     * 
     * @param nr Composante rouge.
     * @param ng Composante vert.
     * @param nb Composante bleu.
    */
    Pixel (unsigned char nr, unsigned char ng, unsigned char nb);
};

#endif